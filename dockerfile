FROM alpine

LABEL maintainer="Ruben Antunes"

WORKDIR /opt/myapp

COPY /equipa-3/cv /opt/myapp

CMD ["cat", "cv"]

